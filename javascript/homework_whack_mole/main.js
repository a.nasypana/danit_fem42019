
class Table {
	constructor (container, row, colom, tableClass, itemClass){
		this.container = container;
		this.row = row;
		this.colom = colom;
		this.tableClass = tableClass;
		this.itemClass = itemClass;
		this.element = null;
	}
	render(){
		this.element = document.createElement("table");
		this.element.className = this.tableClass;
		let str = "";
		let k = 1;
		for (let i = 0; i < this.row; i++){
			str += "<tr>";
			for (let j = 0; j < this.colom; j++){
				str += `<td ` + " " + `class=` +`"${this.itemClass}"><i class="far fa-dot-circle"></i></td>`;
				k++;
			}
			str += "</tr>"
		}
		this.element.innerHTML = str;
		this.container.append(this.element);
		return this.row*this.colom
	};
	delete(){
		this.element.remove()
	}
};

class Level{
	constructor (){
		this.LEVEL = {
			easy: 1500,
			middle: 1000,
			high: 500,
		};
	}
};

class WhackMole extends Level{
	constructor(level, fields, classFields, classUp, classSuccess, classFail, scoreCom, scorePayer, containerMess){
		super();
		this.level = level;
		this.fields = fields;
		this.classFields = classFields;
		this.classUp = classUp;
		this.classSuccess = classSuccess;
		this.classFail = classFail;
		this.scorePl = scorePayer;
		this.scoreCom = scoreCom;
		this.message = containerMess;
		this.arrWaste = [];
		this.computerScore = 0;
		this.playerScore = 0;
		this.startID = 0;
	}
	randomNumber(min, max){
		return Math.round(Math.random()*(max - min) + min)
	};
	change(){
		if(this.arrWaste.length > 0){
			const lastElement = this.arrWaste[this.arrWaste.length-1];
			lastElement.onclick = this.removeEvent.bind(this);
			if(!lastElement.classList.contains(`${this.classSuccess}`)){
				lastElement.classList.remove(`${this.classUp}`)
				lastElement.classList.add(`${this.classFail}`);
				this.computerScore++
				this.scoreCom.innerHTML = this.computerScore
			}
			this.show()
		}else {
			this.show()
		}
	};
	addEvent(event){
		const item = event.target.closest(`.${this.classFields}`);
		item.classList.remove(`${this.classUp}`);
		item.classList.add(`${this.classSuccess}`);
		this.playerScore++;
		this.scorePl.innerHTML = this.playerScore;
	};
	removeEvent(event){
		this.playerScore += 0;
	};
	show(){
		const randomId = this.randomNumber(0, this.fields - 1);
		const randomItem = document.querySelectorAll(`.${this.classFields}`)[`${randomId}`];
		if (this.arrWaste.includes(randomItem)){
			this.show()
		}else {
			randomItem.classList.add(`${this.classUp}`);
			randomItem.onclick = this.addEvent.bind(this);
			// randomItem.addEventListener("click", this.addEvent.bind(this));
			this.arrWaste.push(randomItem);

		}
	};
	move(){
		const time = this.LEVEL[this.level];
		this.startID = setTimeout(()=>{
			this.change();
			if(this.playerScore < this.fields/2 && this.computerScore < this.fields/2){
				this.move()
			}else {
				this.stop();
				if(this.playerScore > this.computerScore){
					this.message.innerHTML = "Congratulations!!! You won."
				}else {
					this.message.innerHTML = "You lost:( Try again!"
				}
			}
		}, time)

	}
	start(){
		this.clean();
		this.move();
	};
	clean(){
		this.message.innerHTML = " ";
		this.scorePl.innerHTML = "0";
		this.scoreCom.innerHTML = "0";
		this.computerScore = 0;
		this.playerScore = 0;
		this.arrWaste = [];
	}
	stop(){
		clearInterval(this.startID);
		const items = document.querySelectorAll(`.${this.classFields}`); ///??? can't apply forEach() to "this.arrWaste"
		for (let i = 0; i <items.length; i++){
			items[i].classList.remove(`${this.classFail}`);
			items[i].classList.remove(`${this.classSuccess}`);
			items[i].classList.remove(`${this.classUp}`);
		};
	}

}

